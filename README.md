# COMPAS_analysis

Repository for my research project on the COMPAS recidivism risk assessment software.  


Code develops the information required to conduct my user study and trains/tests a number of different algorithms to compare with COMPAS.